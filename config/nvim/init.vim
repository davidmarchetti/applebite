" Load Bundles
if filereadable(expand("~/.config/nvim/bundles.vim"))
    source /Users/davidmarchetti/.config/nvim/bundles.vim
endif

if filereadable(expand("~/.config/nvim/bundleconfig.vim"))
    source /Users/davidmarchetti/.config/nvim/bundleconfig.vim
endif

" leader
let mapleader=","
filetype plugin indent on
set clipboard=unnamed

" Less esc
imap jj <Esc>

" General config
scriptencoding utf-8
set backspace=2
set backspace=indent,eol,start
set showcmd
set incsearch
set hlsearch
set ignorecase
set smartcase
set autowrite
set foldenable
syntax on
set hidden
set history=1000
set shortmess+=filmnrxoOtT
set viewoptions=folds,options,cursor,unix,slash
set virtualedit=onemore

" Cursor location fix for git commit messages
au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])

" Colors
set t_Co=256
"let g:hybrid_custom_term_colors = 1
let g:enable_bold_font = 1
colorscheme hybrid_material
set background=dark
let g:airline_theme = "hybrid"
hi LineNr ctermfg=13
hi Normal ctermbg=none
set cursorline

" Line numbers and ruler
set nu
set ruler
set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%)
set showcmd

" Formatting
set autoindent
set shiftwidth=4
set expandtab
set tabstop=4
set softtabstop=4
set nojoinspaces
set splitright
set splitbelow

" Columns not lines for vertical movement
noremap <Down> gj
noremap <Up> gk
noremap j gj
noremap k gk

" Mouse
set mouse=a
set mousehide

" Pane movement more naturally
map <C-J> <C-W>j
map <C-K> <C-W>k
map <C-L> <C-W>l
map <C-H> <C-W>h
tnoremap <C-h> <C-\><C-n><C-w>h
tnoremap <C-j> <C-\><C-n><C-w>j
tnoremap <C-k> <C-\><C-n><C-w>k
tnoremap <C-l> <C-\><C-n><C-w>l

" Airline Status Bar
let g:airline_left_sep='›'
let g:airline_right_sep='‹'
set laststatus=2
set statusline=%<%f\                     " Filename
set statusline+=%w%h%m%r                 " Options
set statusline+=%{fugitive#statusline()} " Git Hotness
set statusline+=\ [%{&ff}/%Y]            " Filetype
set statusline+=\ [%{getcwd()}]          " Current dir
set statusline+=%=%-14.(%l,%c%V%)\ %p%%  " Right aligned file nav info

" Sane terminal emulation keybindings
tnoremap jj <C-\><C-n> 


let g:deoplete#enable_at_startup = 1
let g:SuperTabDefaultCompletionType = "<c-n>"


" Yank from the cursor to the end of the line, to be consistent with C and D.
nnoremap Y y$

" Code folding options
nmap <leader>f0 :set foldlevel=0<CR>
nmap <leader>f1 :set foldlevel=1<CR>
nmap <leader>f2 :set foldlevel=2<CR>
nmap <leader>f3 :set foldlevel=3<CR>
nmap <leader>f4 :set foldlevel=4<CR>
nmap <leader>f5 :set foldlevel=5<CR>
nmap <leader>f6 :set foldlevel=6<CR>
nmap <leader>f7 :set foldlevel=7<CR>
nmap <leader>f8 :set foldlevel=8<CR>
nmap <leader>f9 :set foldlevel=9<CR>
nnoremap <silent> <leader>tt :TagbarToggle<CR>


" Autoenter directory of buffer
autocmd BufEnter * if bufname("") !~ "^\[A-Za-z0-9\]*://" | lcd %:p:h | endif
