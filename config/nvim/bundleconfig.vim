" Tagbar
let g:tagbar_type_markdown = {
    \ 'ctagstype' : 'markdown',
    \ 'kinds' : [
        \ 'h:headings'
    \ ],
\ 'sort' : 0,
\ }

" Neomake
autocmd! BufWritePost * Neomake

